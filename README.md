# Michigan State University's Ballroom Dance Team Website

# THIS REPO IS BEING DEPRECIATED
The free file hosting previously provided by the University will be disabled. This provided a good motivation to move our hosting and rebuild our website for the 2019 school year. [Please see the new updated repo here](https://gitlab.msu.edu/msu-ballroom-dance-team/website)

---

*Live website is available at [msu.edu/~ballroom/](msu.edu/~ballroom/)*

*Webpage preview builds hosted on personal domain: [hsgraduatesguide.com/ballroom](hsgraduatesguide.com/ballroom)*


Here are all the files involved in the development of the new Michigan State University Ballroom Dance Team Website (2016) created by Brendan Carpio.


Most of the files and folders here will not need to be touched as they are simply dependencies. The primary file to be edited will be `index.html` as it contains almost the entirety of the one-page website.


This website is written in HTML5 and CSS3 using the Bootstrap 3 framework to add all the fancy features including artistic elements and responsive UI. The general design of the site is a single webpage with large pictures showing off how fun the team is, introductions of the executive board, and a small area jam-packed with information for new members. It should be nice to view on all sizes of screens, have easy to access links to our social media pages, and, most of all, not look like it's from 2005 as the previous website did.


If you are reading this on a git repository, you are likely savvy in terms of web-development. In that case, feel free to edit what you feel comfortable with. Just, please make sure you make a backup, both locally to you computer and to this git repository, before you start applying changes.


If you aren't so knowledgeable in web-development, don't worry. This website has been / is being designed to be mostly self sufficient and require very little editing of the HTML files. The few times that you will need to edit the files, I will have written documentation giving you step by step instructions on how to perform all the edits.

**If you are editing the website your best resource is the [WIKI](https://gitlab.com/bfcarpio/MSUBDT-Website/wikis/home)**

---

### Pre-Brendan Websites
**3/14/2017**
I've just added the old website, in its entirety, to the git repository. Feel free to poke around there and look at the hidden designs and pictures. I've copied most of the relevant pictures to the archives, but there are general pictures in there.


DO NOT DELETE THE OLD WEBSITE as it serves as the ultimate backup in-case this new site fails.
