# #README - pictures-archive

Inside this folder are all the pictures that I could find of the team. As the years go on and more events happen, I'd like to continue adding entries to supplement the Google archive. 

With the dual structure, it would make modifying the website to include said pictures much easier.

At the moment, most of these pictures are being hosted in groups on the gallery.